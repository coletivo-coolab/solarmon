# SolarMon

Monitoramento de soluções de energia solar para uso em redes comunitarias e nodos offgrid.

## Hardware

O hardware utilizado na solução em testes está composto por:

- raspberry pi 4B 4gbRAM e SD 32GB classe 10
- roteador CPE210 como carga e também transporte
- controladora Volt MPPT [2]
- placa solar 100w, bateria 30Ah, componente step up 12V-> 24V, injetor POE [3]

A lista de equipamentos utilizados assim como a representaçào conceitual e gráfica para montagem da solução segue abaixo:


![grafico] (midia/GraficoMontagemSolar.png "Gráfico")


![imagens] (midia/ConceitoMontagemSolar.png)

 
## Softwware

Monitorar solução em uso, utilizando a pilha com sistema debian, conteiners docker para influxdb como banco de dados, grafana como gerenciador de dashboards e telegraf como agente coletor da [controladora solar](http://www.volt.ind.br/ctrlmpptsolarev.php) que possui dados disponiveis via SNMP.

Manter compatibilidade com o monitoramento de malha [meshmon](https://gitlab.com/rtroian/meshmon/)  usando mesma pilha e collectd.


## implementação

O passo a passo sobre como foi ativar e testar a soluçào segue no documento chamado DetalhesPilha.md


