# Detalhes da pilha

[[_TOC_]]

Notas técnicas sobre o processo de ativar todas as partes  da solução.

## Equipamento Externo

Parte do hardware solar e caixa hermetica para controladora e stepup + POE 

### Controladora MPPT VOLT

O uso desse equipamento está planejado para ser conectado a rede por uma CPE210,que por sua vez é a própria carga da controladora, sendo um sistema fechado offgrid.

Mas para primeiro acesso, precisamos alterar algumas configurações de rede.


#### user padrao

	admin
	voltvolt

#### endereço rede 

	192.168.0.34
	gw 192.168.0.1

#### primeiro acesso 

- fixar ip PC, 192.168.0.10
- conectar diretamente a controladora
- testar com 
	ping 192.168.0.34

se não estiver funcionando, reiniciar modo fabrica:

#### reiniciar modo de fabrica

usando o botão funcao na controladora

	1 seg. liga e desliga saida
	10 segs. reset fabrica
 
 
### Iniciar os trabalhos

- conectar e mudar ip
	- ip novo 10.13.13.13 (usando padrao nome libremesh)
	- gw 10.13.0.1
	- dns1 10.13.0.1
	- dns2 8.8.8.8

salvar, sair e conectar a controladora a malha, agora todas estão no mesma faixa de ip.

testar com ping 10.13.13.13 (ou o ip que foi colocado como fixo)

### Acessar pelo ambiente  web

pelo endereço que  fixamos, estando dentro da malha também, vamos copiar algumas  infos  painel para referência:


```
Configuracões Interface Ethernet

Host 	MPPT
MAC Adress 	54:10:EC:25:85:85
IP 	10.13.13.13
Mascara de Subrede: 	255.255.0.0
Gateway 	10.13.0.1
DNS Primário 	10.13.0.1
DNS Secundário 	8.8.8.8

Informacões do Controlador
Versão WebPage	2.0.0
Versão Hardware	160B01P02
Versão Volt OS	2.0.0
Versão MCU	REV3
Uptime	0d - 00:13
Temperatura 	32 °C
Modo de Operação 	Painel

Painel
Tensão do Painel [VDC] 	16. 2 V
Corrente do Painel [A] 	0. 1 A

Bateria
Tensão da Bateria [VDC] 	13. 1 V
Corrente da Bateria [A] 	0. 1 A
Status da bateria 	Carregando

Saída
Tensão de Saída [VDC] 	13. 1 V
Corrente de Saída [A] 	0. 2 A

Controle Saída
ID 	Status 	Controle
Saída
	
LIGADO
	
```

### testar conexão pelo servidor a base snmp

snmpwalk



##Telegraf

Parte  que recolhe os dados na controladora  e  entrega ao BD para futuroi uso do dashboard

### config

parte 	que colocamos de SNMP

```


# # Retrieves SNMP values from remote agents
[[inputs.snmp]]
  agents = [ "10.148.148.148" ]
  version = 2
  community = "public"
  interval = "60s"
  timeout = "10s"
  retries = 3
  
# sempre o mesmo, just checking ;)
    [[inputs.snmp.field]]
    name = "name"
    oid = "iso.3.6.1.4.1.17095.1.1.1.0"
    is_tag = true
    
# em Celsius
    [[inputs.snmp.field]]
    name = "temperatura"
    oid = "iso.3.6.1.4.1.17095.1.3.1.0"

# modooperacao INTEGER { bateria(0),painel(1),painelbateria(2),standby(3),curto(4),sobrecarga(5)
    [[inputs.snmp.field]]
    name = "modooperacao"
    oid = "iso.3.6.1.4.1.17095.1.3.2.0"
    
# tensaopainel (0..520)
    [[inputs.snmp.field]]
      name = "tensaopainel"
      oid = "iso.3.6.1.4.1.17095.1.3.3.0"
     
# correntepainel INTEGER (0..300)
    [[inputs.snmp.field]]
      name = "correntepainel"
      oid = "iso.3.6.1.4.1.17095.1.3.4.0"
      
# tensaobateria (0..320)
    [[inputs.snmp.field]]
      name = "tensaobateria"
      oid = "iso.3.6.1.4.1.17095.1.3.5.0"
 
# correntebateria (0..400)   
    [[inputs.snmp.field]]
      name = "correntebateria"
      oid = "iso.3.6.1.4.1.17095.1.3.6.0"  
       
# statusbateria INTEGER { descarregando(0),carregando(1),descarregada(2),carregada(3)}   
    [[inputs.snmp.field]]
      name = "statusbateria"
      oid = "iso.3.6.1.4.1.17095.1.3.7.0"   

# tensaosaida INTEGER (0..500)
    [[inputs.snmp.field]]
      name = "tensaosaida"
      oid = "iso.3.6.1.4.1.17095.1.3.8.0"
      
      
# correntesaida INTEGER (0..300)
    [[inputs.snmp.field]]
      name = "correntesaida"
      oid = "iso.3.6.1.4.1.17095.1.3.9.0"


# contrelesaida INTEGER { desligada(0),ligada(1)}
    [[inputs.snmp.field]]
      name = "controlesaida"
      oid = "iso.3.6.1.4.1.17095.1.3.10.0"


# [[inputs.snmp]]
#   agents = [ "10.148.148.148:161" ]
#   ## Timeout for each SNMP query.
#   timeout = "5s"
#   ## Number of retries to attempt within timeout.
#   retries = 3
#   ## SNMP version, values can be 1, 2, or 3
#   version = 2
#
#   ## SNMP community string.
#   community = "public"
#
#   ## The GETBULK max-repetitions parameter
#   max_repetitions = 10
#
#   ## SNMPv3 auth parameters
#   #sec_name = "myuser"
#   #auth_protocol = "md5"      # Values: "MD5", "SHA", ""
#   #auth_password = "pass"
#   #sec_level = "authNoPriv"   # Values: "noAuthNoPriv", "authNoPriv", "authPriv"
#   #context_name = ""
#   #priv_protocol = ""         # Values: "DES", "AES", ""
#   #priv_password = ""
#
#   ## measurement name
#   name = "system"
#   [[inputs.snmp.field]]
#     name = "hostname"
#     oid = ".1.0.0.1.1"
#   [[inputs.snmp.field]]
#     name = "uptime"
#     oid = ".1.0.0.1.2"
#   [[inputs.snmp.field]]
#     name = "load"
#     oid = ".1.0.0.1.3"
#   [[inputs.snmp.field]]
#     oid = "HOST-RESOURCES-MIB::hrMemorySize"
#

#[[inputs.snmp.table]]
#    ## measurement name
#     name = "remote_servers"
#   [[inputs.snmp.table]]
#     ## measurement name
#     name = "remote_servers"
#     inherit_tags = [ "hostname" ]
#     [[inputs.snmp.table.field]]
#       name = "server"
#       oid = ".1.0.0.0.1.0"
#       is_tag = true
#     [[inputs.snmp.table.field]]
#       name = "connections"
#       oid = ".1.0.0.0.1.1"
#     [[inputs.snmp.table.field]]
#       name = "latency"
#       oid = ".1.0.0.0.1.2"
#
#   [[inputs.snmp.table]]
#     ## auto populate table's fields using the MIB
#     oid = "HOST-RESOURCES-MIB::hrNetworkTable"


# # DEPRECATED! PLEASE USE inputs.snmp INSTEAD.
# [[inputs.snmp_legacy]]
#   ## Use 'oids.txt' file to translate oids to names
#   ## To generate 'oids.txt' you need to run:
#   ##   snmptranslate -m all -Tz -On | sed -e 's/"//g' > /tmp/oids.txt
#   ## Or if you have an other MIB folder with custom MIBs
#   ##   snmptranslate -M /mycustommibfolder -Tz -On -m all | sed -e 's/"//g' > oids.txt
#   snmptranslate_file = "/tmp/oids.txt"
#   [[inputs.snmp.host]]
#     address = "192.168.2.2:161"
#     # SNMP community
#     community = "public" # default public
#     # SNMP version (1, 2 or 3)
#     # Version 3 not supported yet
#     version = 2 # default 2
#     # SNMP response timeout
#     timeout = 2.0 # default 2.0
#     # SNMP request retries
#     retries = 2 # default 2
#     # Which get/bulk do you want to collect for this host
#     collect = ["mybulk", "sysservices", "sysdescr"]


```



## InfluxDb
Parte da pilha responsável por armazenar os dados, banco especializado em séries temporais.

Nessa parte do processo não necessita nenhuma configuração além da  ja executada para o meshmon, que é basicamente abrir a porta
### listando bancos

```bash
> show databases

name: databases
name

_internal
collectd
telegraf
influx_telegraf
> 
```


### acessando bd:

```bash
> use telegraf
Using database telegraf
```

### mostrando medidas 

medidas são as unidades que estamos monitorando, que contem por sua vez timestamp, tagset e fieldset

```bash
> show measurements
name: measurements
name
...
**snmp**
```

### mostrando tags

```bash
> show tag keys
name: snmp
tagKey
------
agent_host
host
name
```

### mostrando os dados tags values

```bash
>show series 
...
snmp,agent_host=10.148.148.148,host=13eea6a656a4,name=Controlador/ de/ Carga/ MPPT
```



### campos

```bash
> show field keys
name: snmp
fieldKey        fieldType
--------        ---------
controlesaida   integer
correntebateria integer
correntepainel  integer
correntesaida   integer
modooperacao    integer
statusbateria   integer
temperatura     integer
tensaobateria   integer
tensaopainel    integer#
tensaosaida     integer

```


## selecionando ponto a la SQL

```bash
> select * from snmp limit 2

name: snmp
time                agent_host   host            
--------- ----------------- ----------------- 
1569376710000000000 172.31.41.29 ip-172-31-41-29 ip-172-31-41-29 1    
1569376710000000000 172.31.41.29 ip-172-31-41-29 ip-172-31-41-29 2   
....

> select temperatura from snmp limit 2
name: snmp


time                temperatura
----                -----------
1576945063000000000 40
1576945120000000000 40

```


## Grafana 

Como essa controladora é brasileira, não existe um dashboard pronto para ela no repositório, 
será necessário criar um dashboard personalizado e apos isso compartilhar o código.



